Used to detect signs by passing the dataset through a convolution neural network. Implemented in python using tensorflow.
The rectified linear unit is used as the activation function in convolution layers. Detects signs from 0-5.

Train Accuracy: 0.830556 Test Accuracy: 0.75